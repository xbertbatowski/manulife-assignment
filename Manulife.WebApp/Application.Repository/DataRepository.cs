﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.DAL;
using Application.Repository.Models;

namespace Application.Repository
{
    public class DataRepository : IDisposable
    {
        private masterEntities dbCon = new masterEntities();

        public IEnumerable<SiteRankingModel> GetandFilter(SearchModel model, string order)
        {
            var result = dbCon.SiteRanking.Where(q => (q.SiteUrl.ToLower().Contains(model.siteName) || model.siteName == null || model.siteName == "") &&
                                            ((q.DateGathered >= model.dateFrom || model.dateFrom == null) &&
                                            (q.DateGathered <= model.dateTo || model.dateTo == null)));

            if (model.type > 0)
                result = result.Take(model.type);

            if (order == "up")
                result = result.OrderBy(q => q.VisitCount);
            else if (order == "down")
                result = result.OrderByDescending(q => q.VisitCount);

            var finalResult = result.ToList();
            var retResult = finalResult.Select((r, i) => new SiteRankingModel
            {
                date = r.DateGathered.Value.ToShortDateString(),
                rank = i + 1,
                siteName = r.SiteUrl,
                visits = (int)r.VisitCount
            }).AsEnumerable();

            return retResult;
        }


        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dbCon != null)
                {
                    dbCon.Dispose();
                    dbCon = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
