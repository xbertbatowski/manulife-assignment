﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repository.Models
{
    public class SiteRankingModel
    {
        public int rank { get; set; }
        public string siteName { get; set; }
        public int visits { get; set; }
        public string date { get; set; }
    }
}
