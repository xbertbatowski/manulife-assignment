﻿angular
    .module('siteRankApp')
        .controller('SiteSearchController', ['$scope', '$http', function ($scope, $http) {
            $scope.searchModel = {};
            $scope.ranking = 'down';
            $scope.loading = false;

            //pagination
            $scope.curPage = 0;
            $scope.pageSize = 5;

            $scope.searchRanking = function () {
                $scope.loading = true;
                //console.log($scope.loading);

                //fix for paging bug
                $scope.curPage = 0;
                
                $http.get('/api/ranksearch', { params: {sorder: $scope.ranking, search: angular.toJson($scope.searchModel, false) } })
                    .success(function (response) {
                        $scope.sites = response;
                    })
                    .error(function (data) {
                        console.log("Error: ", data);
                    })
                    .finally(function () {
                        $scope.loading = false;
                        $scope.numberOfPages();
                        //console.log($scope.loading);
                    });
            }

            $scope.changeRanking = function () {
                if ($scope.ranking == 'down') {
                    $scope.ranking = 'up';
                } else {
                    $scope.ranking = 'down';
                }

                $scope.searchRanking();
            }

            $scope.numberOfPages = function () {
                if (!$scope.pageSize || !$scope.sites || !$scope.sites.length) { return; }

                console.log($scope.pageSize);
                return Math.ceil($scope.sites.length / $scope.pageSize);
            };
        }]);