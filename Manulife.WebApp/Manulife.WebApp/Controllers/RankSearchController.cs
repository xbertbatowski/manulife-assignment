﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Application.Repository;
using Application.Repository.Models;
using Newtonsoft.Json;

namespace Manulife.WebApp.Controllers
{
    public class RankSearchController : ApiController
    {
        DataRepository _repository = new DataRepository();

        [HttpGet]
        public IEnumerable<SiteRankingModel> Get(string sorder, string search)
        {
            var searchCriteria = JsonConvert.DeserializeObject<SearchModel>(search);

            if (searchCriteria == null)
                searchCriteria = new SearchModel();
            else
            {
                if (searchCriteria.dateFrom != null)
                {
                    //fix for html5 subtracting 1 day from the selected date
                    searchCriteria.dateFrom = searchCriteria.dateFrom.Value.AddDays(1);
                    searchCriteria.dateFrom = new DateTime(searchCriteria.dateFrom.Value.Date.Year, searchCriteria.dateFrom.Value.Date.Month,
                                                                searchCriteria.dateFrom.Value.Date.Day, 0, 0, 0);
                }
                if (searchCriteria.dateTo != null)
                {
                    //fix for html5 subtracting 1 day from the selected date
                    searchCriteria.dateTo = searchCriteria.dateTo.Value.AddDays(1);
                    searchCriteria.dateTo = new DateTime(searchCriteria.dateTo.Value.Date.Year, searchCriteria.dateTo.Value.Date.Month,
                                                                searchCriteria.dateTo.Value.Date.Day, 23, 59, 0);
                }
            }

            return _repository.GetandFilter(searchCriteria, sorder);
        }

    }
}
