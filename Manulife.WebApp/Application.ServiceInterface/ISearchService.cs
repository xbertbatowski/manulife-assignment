﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.ServiceInterface.Model;
using Application.ServiceInterface.Model.Search;

namespace Application.ServiceInterface
{
    public interface ISearchService
    {
        IEnumerable<SiteRankingModel> SearchData(int page, int pageSize, RankSearchModel search);
    }
}
