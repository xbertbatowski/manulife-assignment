﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ServiceInterface.Model
{
    public class SiteRankingModel
    {
        public int Id { get; set; }
        public string url { get; set; }
        public Nullable<int> visists { get; set; }
        public Nullable<System.DateTime> date { get; set; }
    }
}
