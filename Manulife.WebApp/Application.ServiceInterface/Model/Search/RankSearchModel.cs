﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Application.ServiceInterface.Model.Search
{
    public class RankSearchModel
    {
        [Display(Name = "Website Name/Url")]
        public string siteName { get; set; }

        [Display(Name = "Date From")]
        public DateTime? dateFrom { get; set; }

        [Display(Name = "Date To")]
        public DateTime? dateTo { get; set; }
        public int visitCount { get; set; }
    }
}
