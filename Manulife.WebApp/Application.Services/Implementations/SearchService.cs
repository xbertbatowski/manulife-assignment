﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.ServiceInterface;
using Application.ServiceInterface.Model;
using Application.ServiceInterface.Model.Search;
using Application.DAL;

namespace Application.Services.Implementations
{
    public class SearchService :  ISearchService
    {
        public IEnumerable<SiteRankingModel> SearchData(int page, int pageSize, RankSearchModel search)
        {
            using (var dbCon = new dbEntities())
            {
                var result = dbCon.SiteRankings.Where(q => 
                            (q.SiteUrl.ToLower().Contains(search.siteName.ToLower()) || search.siteName == null || search.siteName == "") &&
                            (q.DateGathered >= search.dateFrom || search.dateFrom == null) &&
                            (q.DateGathered <= search.dateTo || search.dateTo == null))
                            .OrderByDescending(q => q.VisitCount);

                return result.Select(q => new SiteRankingModel 
                {
                    Id = q.Id,
                    url = q.SiteUrl,
                    date = q.DateGathered,
                    visists = q.VisitCount
                }).AsEnumerable();
            }
        }
    }
}